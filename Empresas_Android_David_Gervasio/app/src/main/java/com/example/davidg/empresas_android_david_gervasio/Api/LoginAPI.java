package com.example.davidg.empresas_android_david_gervasio.Api;

import com.example.davidg.empresas_android_david_gervasio.Model.LoginModel;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by davidg on 11/09/18.
 */

public interface LoginAPI {

    @POST("users/auth/sign_in")
    Call< Object> sign_in(@Body LoginModel user);

}