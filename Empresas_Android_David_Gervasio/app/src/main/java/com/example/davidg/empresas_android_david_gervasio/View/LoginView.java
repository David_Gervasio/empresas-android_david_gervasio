package com.example.davidg.empresas_android_david_gervasio.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.davidg.empresas_android_david_gervasio.Controller.LoginController;
import com.example.davidg.empresas_android_david_gervasio.R;

//Obs: foi utilizado o padrão MVC
//tela de login
public class LoginView extends AppCompatActivity {

    private LoginController loginController;
    private EditText etxEmail, etxPassword;
    private Button btEnter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_view);

        this.etxEmail = (EditText) this.findViewById(R.id.editText_email);
        this.etxPassword = (EditText) this.findViewById(R.id.editText_password);
        this.btEnter = (Button) this.findViewById(R.id.button_enter);

        loginController = new LoginController(this);

        //verificar se usuario esta logado
        this.checkLogin();

        //ler imputs para login e efetua-lo
        btEnter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                if (etxEmail.getText() != null || etxPassword.getText() != null) {

                    LoginView.this.makeLogin(etxEmail.getText().toString(), etxPassword.getText().toString());

                } else {
                    //menssagem
                    Toast.makeText(LoginView.this, "Preencha todos os campos!", Toast.LENGTH_SHORT).show();
                }

            }

        });

    }

    //chama loginController.checkLogin() para verificar se o usuario se encontra logado
    public void checkLogin() {
        loginController.checkLogin();
    }

    //chama loginController.makeLogin para realizar o login
    public void makeLogin(String email, String password) {
          loginController.makeLogin(email, password);
    }

}
