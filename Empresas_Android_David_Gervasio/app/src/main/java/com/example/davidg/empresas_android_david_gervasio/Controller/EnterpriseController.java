package com.example.davidg.empresas_android_david_gervasio.Controller;

import android.content.Context;
import android.util.Log;

import com.example.davidg.empresas_android_david_gervasio.Model.EnterpriseListResponse;
import com.example.davidg.empresas_android_david_gervasio.Model.EnterpriseModel;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by davidg on 11/09/18.
 */

public class EnterpriseController {
    private Context context;
    private EnterpriseModel enterpriseModel;

    public EnterpriseController(Context context) {
        this.context = context;
    }

    //Listagem de Empresas
    public String getEnterprisesList() {
        enterpriseModel = new EnterpriseModel(context);
        EnterpriseListResponse enterpriseListResponse = enterpriseModel.getEnterprisesList();

        //motar array de json
        Gson gson = new Gson();

        // converte objeto EnterpriseListResponse para JSON e retorna JSON como String para a EnterpriseView
        String listjson = gson.toJson(enterpriseListResponse);

        //Log.d("script", "json -> " + json);
        Log.d("script", "lisjson -> " + listjson);
        return listjson;
    }

}
