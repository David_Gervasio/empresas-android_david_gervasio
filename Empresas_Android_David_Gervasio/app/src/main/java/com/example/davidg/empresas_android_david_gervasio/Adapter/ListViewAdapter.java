package com.example.davidg.empresas_android_david_gervasio.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.davidg.empresas_android_david_gervasio.Model.EnterpriseModel;
import com.example.davidg.empresas_android_david_gervasio.R;
import com.example.davidg.empresas_android_david_gervasio.View.EnterpriseDetailsView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.example.davidg.empresas_android_david_gervasio.R.id.imageView;

/**
 * Created by davidg on 11/09/18.
 */

public class ListViewAdapter extends BaseAdapter {

    private ArrayList<JSONObject> arrayList;
    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<JSONObject> jsonObjects;


    public ListViewAdapter(Context context, ArrayList<JSONObject> jsonObjects) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.jsonObjects = jsonObjects;
        this.arrayList = new ArrayList<JSONObject>();
        this.arrayList.addAll(jsonObjects);


    }

    public class ViewHolder {
        TextView txname, txcontry, txtype;
        ImageView mIconIv;
    }

    @Override
    public int getCount() {
        return jsonObjects.size();
    }

    @Override
    public Object getItem(int i) {
        return jsonObjects.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int postition, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.row_enterprise, null);

            //locate the views in row.xml
            holder.txname = (TextView) view.findViewById(R.id.enterprise_name);
            holder.txcontry = (TextView) view.findViewById(R.id.country);
            holder.txtype = (TextView) view.findViewById(R.id.type);
            holder.mIconIv = (ImageView) view.findViewById(R.id.imgE);
            //holder.mIconIv = view.findViewById(R.id.mainIcon);

            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        //set the results into textviews
        try {
            holder.txname.setText(jsonObjects.get(postition).getString("enterprise_name"));
            holder.txcontry.setText(jsonObjects.get(postition).getString("country"));
            holder.txtype.setText(jsonObjects.get(postition).getJSONObject("enterprise_type").getString("enterprise_type_name"));
            holder.txtype.setText(jsonObjects.get(postition).getJSONObject("enterprise_type").getString("enterprise_type_name"));
            // GlideApp.with(this).load("http://goo.gl/gEgYUd").into(imageView);

            Picasso.with(mContext)
                    .load("http://empresas.ioasys.com.br" + jsonObjects.get(postition).getString("photo"))
                    .into(holder.mIconIv);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        //adicionar evento
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //code later
                //start NewActivity with title for actionbar and text for textview
                Intent intent = new Intent(mContext, EnterpriseDetailsView.class);
                try {
                    intent.putExtra("enterprise_name", jsonObjects.get(postition).getString("enterprise_name"));
                    intent.putExtra("description", jsonObjects.get(postition).getString("description"));
                    intent.putExtra("photo", jsonObjects.get(postition).getString("photo"));
                    mContext.startActivity(intent);

                } catch (JSONException e) {


                    //Inicia EnterpriseDetailsView mesmo quando a empresa não pussuir foto;
                    try {
                        intent.putExtra("enterprise_name", jsonObjects.get(postition).getString("enterprise_name"));
                        intent.putExtra("description", jsonObjects.get(postition).getString("description"));
                        mContext.startActivity(intent);

                    } catch (JSONException f) {
                        f.printStackTrace();
                    }

                    e.printStackTrace();
                }


            }
        });


        return view;
    }

    //filtrar
    public void filter(String charText) throws JSONException {
        charText = charText.toLowerCase(Locale.getDefault());
        jsonObjects.clear();
        if (charText.length() == 0) {
            jsonObjects.addAll(arrayList);
        } else {
            for (JSONObject e : arrayList) {
                if (e.getString("enterprise_name").toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    jsonObjects.add(e);
                }
            }
        }
        notifyDataSetChanged();
    }

}
