package com.example.davidg.empresas_android_david_gervasio.Controller;

import android.content.Context;

import com.example.davidg.empresas_android_david_gervasio.Model.LoginModel;

/**
 * Created by davidg on 11/09/18.
 */

public class LoginController {

    private LoginModel loginModel;

    public LoginController(Context context) {
        loginModel = new LoginModel(context);
    }

    public void checkLogin() {
        loginModel.checkLogin();
    }

    public void makeLogin(String email, String password) {
        loginModel.makeLogin(email, password);
    }

}
