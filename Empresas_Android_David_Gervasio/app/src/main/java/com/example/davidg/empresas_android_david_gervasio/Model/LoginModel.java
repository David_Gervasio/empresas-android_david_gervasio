package com.example.davidg.empresas_android_david_gervasio.Model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.example.davidg.empresas_android_david_gervasio.Api.LoginAPI;
import com.example.davidg.empresas_android_david_gervasio.View.EnterpriseView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by davidg on 11/09/18.
 */

public class LoginModel {

    private static final String PREF_NAME = "AppSharedPreferences";
    private final String URL_BASE = "http://empresas.ioasys.com.br/api/v1/";
    private String email;
    private String password;
    private Retrofit retrofit;
    private Context context;

    //construtor1
    public LoginModel(Context context) {

        this.context = context;

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .addConverterFactory(GsonConverterFactory.create());

        retrofit = builder.build();

    }
    //construtor2
    public LoginModel(String email, String password) {

        this.email = email;
        this.password = password;
    }


    //checar login
    public void checkLogin() {

        //verificar se ha  email e senha no sharedPreferences e realizar o login
        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        String email = sp.getString("email", "");
        String password = sp.getString("password", "");

        if (!email.equals("") && (!password.equals(""))) {
             this.makeLogin(email, password);
        }
    }


    //fazer login
    public void makeLogin(String email, String password) {

        //amazenar senha e email no sharedPreferences
        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("email", email);
        editor.putString("password", password);
        editor.commit();

        //realiza a requizição no servidor de forma assincrona
        LoginAPI loginAPI = retrofit.create(LoginAPI.class);
        LoginModel user = new LoginModel(email, password);
        Call<Object> call = loginAPI.sign_in(user);

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                Object access = response.body();
                okhttp3.Headers headers = response.headers();
                Log.d("script", "onResponse: header " + headers.get("access-token"));

                if (response.code() == 200) {

                    //amazenar access-token, uid e client no sharedPreferences
                    SharedPreferences sp = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("access-token", headers.get("access-token"));
                    editor.putString("uid", headers.get("uid"));
                    editor.putString("client", headers.get("client"));
                    editor.commit();

                    Intent intent = new Intent(LoginModel.this.context, EnterpriseView.class);
                    LoginModel.this.context.startActivity(intent);


                } else {

                    Toast toast = Toast.makeText(context, "Dados não conferem",Toast.LENGTH_SHORT);
                    toast.show();

                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.d("script", "onFailure:" + t.toString());
            }

        });


    }


    @Override
    public String toString() {
        return "User{" + "email='" + email + '\'' + ",password='" + password + '\'' + '}';

    }


}
