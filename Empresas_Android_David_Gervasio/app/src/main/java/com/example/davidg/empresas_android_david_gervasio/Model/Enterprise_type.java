package com.example.davidg.empresas_android_david_gervasio.Model;

import java.io.Serializable;

/**
 * Created by davidg on 11/09/18.
 */

//exibi detalhes sobre uma empresa
public class Enterprise_type  implements Serializable {

    int id;
    String enterprise_type_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Enterprise_type{" +
                "id=" + id +
                ", enterprise_type_name='" + enterprise_type_name + '\'' +
                '}';
    }
}