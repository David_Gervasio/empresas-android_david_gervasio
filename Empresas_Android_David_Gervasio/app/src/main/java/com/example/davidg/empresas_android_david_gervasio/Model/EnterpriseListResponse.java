package com.example.davidg.empresas_android_david_gervasio.Model;

import java.util.List;

/**
 * Created by davidg on 11/09/18.
 */

public class EnterpriseListResponse {

    public List<EnterpriseModel> enterprises;
}
