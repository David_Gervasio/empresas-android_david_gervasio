package com.example.davidg.empresas_android_david_gervasio.View;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.davidg.empresas_android_david_gervasio.Adapter.ListViewAdapter;
import com.example.davidg.empresas_android_david_gervasio.Controller.EnterpriseController;
import com.example.davidg.empresas_android_david_gervasio.Controller.LoginController;
import com.example.davidg.empresas_android_david_gervasio.Model.EnterpriseModel;
import com.example.davidg.empresas_android_david_gervasio.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


//exibe lista de empresas
public class EnterpriseView extends AppCompatActivity {

    private EnterpriseController enterpriseController;
    private ListView listView;
    ListViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise_view);

        listView = (ListView) findViewById(R.id.listView);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(" ");
        //actionBar.setLogo(R.drawable.logo_nav);
        listView = (ListView) findViewById(R.id.listView);

        getEnterprisesList();


    }

    //solicita ao EnterpriseController.getEnterprisesList() os dados de todas as empresas no formato json;
    public void getEnterprisesList() {
        enterpriseController = new EnterpriseController(this);
        String json = enterpriseController.getEnterprisesList();
        Log.d("scrpit", " json  getEnterprisesList  " + json);
        ArrayList<JSONObject> arrayList = new ArrayList<JSONObject>();


        try {
            JSONObject jsonObject = new JSONObject(json);
            //montar ou arrya de jsonObject
            JSONArray enterprises = jsonObject.getJSONArray("enterprises");

            //mota array de JSONObject
            for (int i = 0; i < enterprises.length(); i++) {
                JSONObject enterprise = enterprises.getJSONObject(i);
                arrayList.add(enterprise);
            }

            //passa array para adapter
            adapter = new ListViewAdapter(this, arrayList);

            //ligar adpter ao list view
            listView.setAdapter(adapter);


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    //menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                if (TextUtils.isEmpty(s)) {
                    try {
                        adapter.filter("");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    listView.clearTextFilter();
                } else {
                    try {
                        adapter.filter(s);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                return true;
            }
        });
        return true;
    }


}
