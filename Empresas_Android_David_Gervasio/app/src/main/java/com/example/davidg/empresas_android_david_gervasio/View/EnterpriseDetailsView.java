package com.example.davidg.empresas_android_david_gervasio.View;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.davidg.empresas_android_david_gervasio.R;
import com.squareup.picasso.Picasso;

//EnterpriseDetailsView exibe uma foto e detalhes sobre uma empresa

public class EnterpriseDetailsView extends AppCompatActivity {
    private String name;
    private String description;
    private TextView textView;
    private ImageView imageView;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise_details_view);


        //Recebendo parametros de acesso
        Intent intent = getIntent();
        name = intent.getStringExtra("enterprise_name");
        description = intent.getStringExtra("description");
        url = intent.getStringExtra("photo");
        imageView = (ImageView) findViewById(R.id.imgEnterprise);

        textView = (TextView) findViewById(R.id.txDescription);
        textView.setText(description);

        //baixar imagem
        Picasso.with(this)
                .load("http://empresas.ioasys.com.br" + url)
                .resize(70, 70)
                .into(imageView);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(name.toUpperCase());


    }
}
