package com.example.davidg.empresas_android_david_gervasio.Api;

import com.example.davidg.empresas_android_david_gervasio.Model.EnterpriseListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by davidg on 11/09/18.
 */

public interface EnterpriseAPI {

        //Busca todas as empresas
        @GET("enterprises")
        Call<EnterpriseListResponse> enterprisesList(@Header("access-token") String accessToken, @Header("client") String client, @Header("uid") String uid );


        //Busca uma empresa especifica
        @GET("enterprises?enterprise_types=?&name=")
        Call<EnterpriseListResponse> enterprisesIndexwithFilter(@Header("access-token") String accessToken, @Header("client") String client, @Header("uid") String uid , @Query("enterprise_types") int enterprise_types, @Query("name") String name);


}
