package com.example.davidg.empresas_android_david_gervasio.Model;

import android.content.Context;

import android.content.SharedPreferences;
import android.os.StrictMode;


import com.example.davidg.empresas_android_david_gervasio.Api.EnterpriseAPI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.R.layout.simple_expandable_list_item_1;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by davidg on 11/09/18.
 */

public class EnterpriseModel {

    private String URL_BASE;
    private String accesstoken;
    private String uid;
    private String client;
    private retrofit2.Call call;
    private EnterpriseAPI enterpriseAPI;
    private Context context;
    private static final String PREF_NAME = "AppSharedPreferences";

    //dados da empresa
    private int id;
    private String enterprise_name;
    private String description;
    private String email_enterprise;
    private String facebook;
    private String twitter;
    private String linkedin;
    private String phone;
    private String own_enterprise;
    private String photo;
    private int value;
    private int shares;
    private int share_price;
    private int own_shares;
    private String city;
    private String country;
    private Enterprise_type enterprise_type;


    public EnterpriseModel(Context context) {

        this.context = context;
        URL_BASE = "http://empresas.ioasys.com.br/api/v1/";
        call = null;
        //pegando dados do SharedPreferences para acessar as API
        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        accesstoken = sp.getString("access-token", "");
        uid = sp.getString("uid", "");
        client = sp.getString("client", "");

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        enterpriseAPI = retrofit.create(EnterpriseAPI.class);

        //permition for accept the internt in activit thread
        if (android.os.Build.VERSION.SDK_INT > 0) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


    }


    //metodos
    public String getEnterprise_name() {
        return enterprise_name;
    }

    public String getCountry() {
        return country;
    }

    //busaca no servidor lista de empresas de forma  sincrona
    public EnterpriseListResponse getEnterprisesList() {
        call = enterpriseAPI.enterprisesList(accesstoken, client, uid);
        try {
            EnterpriseListResponse enterpriseListResponse = (EnterpriseListResponse) call.execute().body();

            return enterpriseListResponse;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


}
